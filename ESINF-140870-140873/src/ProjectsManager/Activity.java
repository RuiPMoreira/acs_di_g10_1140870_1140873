package ProjectsManager;

/**
 *
 * @author 1140870_1140873
 */
public class Activity implements Comparable{

    private String projectRef;
    private String code;
    private String type;
    private int durationTime;
    private int delayTime;

    public Activity(String projectRef, String code, String type, int durationTime, int delayTime) {
        this.projectRef = projectRef;
        this.code = code;
        this.type = type;
        this.durationTime = durationTime;
        this.delayTime = delayTime;
    }

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(int durationTime) {
        this.durationTime = durationTime;
    }

    public int getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

     @Override
    public int compareTo(Object o) {
        if(Integer.compare(this.delayTime, ((Activity) o).getDelayTime()) != 0)
            return Integer.compare(this.delayTime, ((Activity) o).getDelayTime());
        else
            if(code.compareToIgnoreCase(((Activity) o).getCode()) !=0)
                return (code.compareToIgnoreCase(((Activity) o).getCode()));
            else
                return projectRef.compareToIgnoreCase(((Activity) o).getProjectRef());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activity other = (Activity) obj;
        if (!this.projectRef.equalsIgnoreCase(other.getProjectRef())) {
            return false;
        }
        if (!this.code.equalsIgnoreCase(other.getCode())) {
            return false;
        }
        if (!this.type.equalsIgnoreCase(other.getType())) {
            return false;
        }
        if (this.durationTime != other.getDurationTime()) {
            return false;
        }
        if (this.delayTime != other.getDelayTime()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Activity ->  " + "Project Reference=" + projectRef + ", Code=" + code + ", Type=" + type + ", Duration Time=" + durationTime + ", Delay Time=" + delayTime;
    }    
}
