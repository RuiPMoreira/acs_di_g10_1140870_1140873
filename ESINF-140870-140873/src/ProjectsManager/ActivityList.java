package ProjectsManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityList {

    private final List<Activity> activites;

    public ActivityList() {
        activites = new ArrayList<>();
    }

    public List<Activity> getActivites() {
        return activites;
    }

    public void add(Activity a) {
        activites.add(a);
    }

}
