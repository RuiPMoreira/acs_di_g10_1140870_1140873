package ProjectsManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 1140870_1140873
 */
public class Files {

    /**
     * lista de atividades
     */
    private final ActivityList activities;
    /**
     * lista de projetos
     */
    private final ProjectList projects;

    /**
     * Contrutor que recebe duas listas como parametro para serem preenchidas
     * com informação lida do ficheiro
     *
     * @param activities
     * @param projects
     */
    public Files(ActivityList activities, ProjectList projects) {
        this.activities = activities;
        this.projects = projects;
    }

    /**
     * Método que recebe como parametro o nome do ficheiro e lê a sua informação
     * para as listas
     *
     * @param file
     * @throws Exception
     */
    public void readFile(String file) throws Exception {
        try (Scanner scanner = new Scanner(new File(file), "UTF-8")) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.length() > 0) {
                    if (line.trim().equalsIgnoreCase("PROJECT")) {
                        treeProject(scanner);
                        break;
                    } else {
                        if (line.trim().equalsIgnoreCase("ACTIVITY")) {
                            treeActivity(scanner);
                            break;
                        }
                    }
                }
            }
            if (!verify()) {
                System.err.println("Erro a ler ficheiro");
                activities.getActivites().clear();
                projects.getProjects().clear();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Não Existe Ficheiro.");
        }
    }

    /**
     * Método que lê os projetos do ficheiro e coloca nas listas de projetos
     *
     * @param scanner
     */
    private void treeProject(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.length() > 0 && !line.trim().equalsIgnoreCase("ACTIVITY")) {
                String[] fields = line.split(",");
                if (fields.length == 4) {
                    Project project = new Project(fields[0].trim(), fields[1].trim(),
                            Integer.parseInt(fields[2].trim()), Integer.parseInt(fields[3].trim()));
                    projects.add(project);
                }
            } else {
                if (line.trim().equalsIgnoreCase("ACTIVITY")) {
                    treeActivity(scanner);
                    break;
                }
            }
        }
    }

    /**
     * Método que lê as Atividades do ficheiro e coloca na lista das atividades
     *
     * @param scanner
     */
    private void treeActivity(Scanner scanner) {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.length() > 0 && !line.trim().equalsIgnoreCase("PROJECT")) {
                String[] fields = line.split(",");
                if (fields.length == 5) {
                    ProjectsManager.Activity activity = new ProjectsManager.Activity(fields[0].trim(), fields[1].trim(),
                            fields[2].trim(), Integer.parseInt(fields[3].trim()), Integer.parseInt(fields[4].trim()));
                    activities.add(activity);
                }
            } else {
                if (line.trim().equalsIgnoreCase("PROJECT")) {
                    treeProject(scanner);
                    break;
                }
            }

        }
    }

    /**
     * Método que verifica se os dados do ficheiro são validos (ex:atividades
     * com projetos que não foram lidos)
     *
     * @return
     */
    private boolean verify() {
        ArrayList<Activity> remove = new ArrayList<>();
        boolean bool = false;
        if (!activities.getActivites().isEmpty() && !projects.getProjects().isEmpty()) {
            for (ProjectsManager.Activity activity : activities.getActivites()) {
                for (ProjectsManager.Project project : projects.getProjects()) {
                    if (activity.getProjectRef().trim().equalsIgnoreCase(project.getReference().trim())) {
                        bool = true;
                        break;
                    } else {
                        bool = false;
                    }
                }
                if (bool == false) {
                    remove.add(activity);
                }
            }
            if (!remove.isEmpty()) {
                for (Activity a : remove) {
                    activities.getActivites().remove(a);
                }
            }
            if (!activities.getActivites().isEmpty() && !projects.getProjects().isEmpty()) {
                verifyDelay();
                if (!activities.getActivites().isEmpty() && !projects.getProjects().isEmpty()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Método que verifica se as delay de todas atividades corresponde ao total
     * de delay do projeto a que corresponde
     */
    private void verifyDelay() {
        int somaDelay;
        for (ProjectsManager.Project project : projects.getProjects()) {
            somaDelay = 0;
            for (ProjectsManager.Activity activity : activities.getActivites()) {
                if (activity.getProjectRef().trim().equalsIgnoreCase(project.getReference().trim())) {
                    somaDelay += activity.getDelayTime();
                }
            }
            if (somaDelay != project.getDelayTime()) {
                activities.getActivites().clear();
                projects.getProjects().clear();
                break;
            }
        }
    }
}
