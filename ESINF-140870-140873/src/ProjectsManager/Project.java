package ProjectsManager;

/**
 *
 * @author 1140870_1140873
 */
public class Project implements Comparable{
    
    private String reference;
    private String type;
    private int completionTime;
    private int delayTime;

    public Project(String reference, String type, int completionTime, int delayTime) {
        this.reference = reference;
        this.type = type;
        this.completionTime = completionTime;
        this.delayTime = delayTime;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(int completionTime) {
        this.completionTime = completionTime;
    }

    public int getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }

    @Override
    public int compareTo(Object o) {
        if(Integer.compare(this.delayTime, ((Project) o).getDelayTime()) != 0)
            return Integer.compare(this.delayTime, ((Project) o).getDelayTime());
        else
            return reference.compareToIgnoreCase(((Project) o).getReference());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
       if (!this.reference.equalsIgnoreCase(other.getReference())) {
            return false;
        }
        if (!this.type.equalsIgnoreCase(other.getType())) {
            return false;
        }
        if (this.completionTime != other.getCompletionTime()) {
            return false;
        }
        if (this.delayTime != other.getDelayTime()) {
            return false;
        }
         return true;
    }

    @Override
    public String toString() {
        return "Project ->  " + "Reference=" + reference + ", Type=" + type + ", Completion Time=" + completionTime + ", Delay=" + delayTime;
    }
}
