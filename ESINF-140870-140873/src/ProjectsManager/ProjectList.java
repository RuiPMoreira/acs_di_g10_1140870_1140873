package ProjectsManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class ProjectList {

    private final List<Project> projects;

    public ProjectList() {
        projects = new ArrayList<>();
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void add(Project p) {
        projects.add(p);
    }
}
