package ProjectsManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class ProjectManager {

    /**
     * Arvore com os projetos
     */
    private BST treeProject;
    /**
     * Arvore com as atividades
     */
    private BST treeActivity;

    /**
     * Contrutor que inicializa as arvores
     */
    public ProjectManager() {
        treeProject = new BST();
        treeActivity = new BST();
    }

    /**
     * Método que recebe como parametro uma lista de projetos e insere numa
     * arvore
     *
     * @param projectos
     */
    public void insertInProjectTree(List<Project> projectos) {
        for (Project p : projectos) {
            treeProject.insert(p);
        }
    }

    /**
     * Método que recebe como parametro uma lista de atividades e insere numa
     * arvore
     *
     * @param atividades
     */
    public void insertInActivityTree(List<Activity> atividades) {
        for (Activity a : atividades) {
            treeActivity.insert(a);
        }
    }

    /**
     * Método que mostras os projetos e suas atividades por ordem de delay do
     * projeto.
     */
    public void displayInOrder() {
        Iterable<Project> projectos = treeProject.inOrder();
        ArrayList<Activity> actividades;

        for (Project p : projectos) {
            System.out.print("Projecto '" + p.getReference() + "': ");

            actividades = getAtivitysOfProject(p.getReference());

            if (actividades.isEmpty()) {
                System.out.println("Não tem atividades.");
            } else {

                System.out.println();
                for (Activity a : actividades) {
                    if (a.getProjectRef().equals(p.getReference())) {
                        System.out.println("          " + a.getCode());
                    }
                }
            }
            System.out.println();
        }
    }

    /**
     * Metodo que recebe como parametro dois projetos, e retorna as actividades
     * com o mesmo tipo.
     *
     * @param projecto1
     * @param projecto2
     * @return
     */
    public ArrayList<Activity> getLatestActivity(Project projecto1, Project projecto2) {
        Iterable<Activity> it = treeActivity.inOrder();
        ArrayList<Activity> actividades = new ArrayList<Activity>();

        if (projecto1.getDelayTime() == 0) {
            return null;
        } else if (projecto2.getDelayTime() == 0) {
            return null;
        }
        for (Activity actividade1 : it) {
            if (actividade1.getProjectRef().trim().equals(projecto1.getReference().trim()) && actividade1.getDelayTime() > 0) {
                for (Activity actividade2 : it) {
                    if (actividade2.getDelayTime() > 0 && actividade2.getType().trim().equals(actividade1.getType().trim()) && actividade2.getProjectRef().trim().equals(projecto2.getReference())) {
                        if (actividades.indexOf(actividade1) == -1 && actividades.indexOf(actividade2) == (-1)) {
                            actividades.add(actividade1);
                            actividades.add(actividade2);
                        }
                    }
                }
            }
        }
        return actividades;
    }

    /**
     * Metodo que recebe a referencia do projeto e devolve o projeto
     *
     * @param ref
     * @return
     */
    private Project getProjectByRef(String ref) {
        Iterable<Project> projectos;
        projectos = treeProject.inOrder();
        for (Project p : projectos) {
            if (ref.equals(p.getReference())) {
                return p;
            }
        }
        return null;
    }

    /**
     * Método que devolve uma lista de atividades de um projeto
     *
     * @param reference
     * @return
     */
    private ArrayList<Activity> getAtivitysOfProject(String reference) {
        ArrayList<Activity> doProjecto = new ArrayList<>();

        if (getProjectByRef(reference) == null) {
            return null;
        }

        Iterable<Activity> actividades;
        actividades = treeActivity.inOrder();
        for (Activity a : actividades) {
            if (a.getProjectRef().equals(reference)) {
                doProjecto.add(a);
            }
        }
        return doProjecto;
    }
}
