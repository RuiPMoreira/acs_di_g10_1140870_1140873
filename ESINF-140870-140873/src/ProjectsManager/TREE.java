package ProjectsManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 *
 * @author 1140870_1140873
 * @param <E>
 */
/*
 public interface:

 public BST()
 public boolean isEmpty()
 public int size()
 public void insert(E element)
 public void remove(E element)
 public String toString()

 public int height()
 public E smallestElement()
 public Iterable<E> inOrder()
 public Iterable<E> preOrder()
 public Iterable<E> postOrder()
 public Map<Integer,List<E>> nodesByLevel()
 ++++++++++++++++++++++++++++++
 public int depth(E element)
 public E parent(E element)
 public boolean contains(E element)
 public boolean isLeaf(E element)
 public Iterator<E> iterator()

 public List<E> ascdes()


 */
public class TREE<E extends Comparable<E>> extends BST<E> {

    //---------------------------------------------------------------   

    /**
     * Returns the number of levels separating E element from the root.
     *
     * @param element A valid element within the tree
     * @return
     * @throws IllegalArgumentException if element is not a valid E for this
     * tree.
     */
    public int depth(E element) {
        return (depth(element, root));
    }

    private int depth(E element, Node<E> node) {
        if(node.getElement().compareTo(element) == 0)
            return 0;
        else if(node.getElement().compareTo(element) > 0)
            return this.depth(element, node.getLeft()) + 1;
        else
            return this.depth(element, node.getRight()) + 1;
    }

    //---------------------------------------------------------------   

    public boolean contains(E element) {
        return this.find(element, root) != null;
    }

 //---------------------------------------------------------------   
    public boolean isLeaf(E element) {
        Node<E> node = this.find(element, root);
        return node != null && node.getLeft() == null && node.getRight() == null;
    }

//---------------------------------------------------------------   
    /**
     * Returns the parent's Element of an Element (or null if Element is the
     * root or not belongs to the tree).
     *
     * @param element A valid element within the tree
     * @return Element of element's parent (or null if element is root)
     */
    public E parent(E element) {
        Node<E> node = this.find(element, root);
        if(node == null || node == root)
            return null;
        
        return parent(node, root);
    }

    private E parent(Node<E> node, Node<E> parentNode) {
        if(parentNode == null)
            return null;
        
        if(parentNode.getRight() == node || parentNode.getLeft()== node)
            return parentNode.getElement();
        
        if(parentNode.getElement().compareTo(node.getElement()) > 0)
            return parent(node, parentNode.getLeft());
        else
            return parent(node, parentNode.getRight());
    }

//****************************************************************
    public Iterator<E> iterator() {
        return new BSTIterator();
    }
//#########################################################################
//#########################################################################
//---------------------------------------------------------------   

    /**
     * Returns the tree without leaves.
     *
     * @return tree without leaves
     */
    public BST<E> autumnTree() {
        TREE<E> tree = new TREE<>();
        
        tree.root = copyRec(root());

        return tree;
    }

    private Node<E> copyRec(Node<E> node) {
        if(node == null)
            return null;
        
        if(node.getLeft() == null && node.getRight() == null)
            return null;
        
        Node<E> novoNode = new Node<>(node.getElement(), copyRec(node.getLeft()), copyRec(node.getRight()));
        
        return novoNode;
    }

//#########################################################################
//#########################################################################  
    private class BSTIterator implements Iterator<E> {

        private final Stack<Node<E>> stack;
        E curElement;       //current element
        boolean canRemove;  //to enable remove()

        public BSTIterator() {
            stack = new Stack<>();
            Node<E> cur = (Node<E>) root();
            while (cur != null) {
                stack.push(cur);
                cur = cur.getLeft();
            }
            curElement = null;
            canRemove = false;
        }

        /**
         * @return whether we have a next smallest element
         */
        @Override
        public boolean hasNext() {
            return !stack.empty();
        }

        /**
         * @return the next smallest element
         */
        @Override
        public E next() {
            Node<E> cur = stack.pop();
            this.curElement = cur.getElement();
            
            cur = cur.getRight();
            while (cur != null) {
                stack.push(cur);
                cur = cur.getLeft();
            }

            this.canRemove = true;
            return curElement;
        }

        /**
         * remove the current element
         */
        @Override
        public void remove() {
            if (canRemove) {
                stack.clear();
                curElement = null;
                canRemove = false;
            } else {
                throw new IllegalStateException();
            }
        }
    }
//#########################################################################
//######################################################################### 

    /**
     * build a list with all elements of the tree. The elements in the left
     * subtree in ascending order and the elements in the right subtree in
     * descending order.
     *
     * @return returns a list with the elements of the left subtree in ascending
     * order and the elements in the right subtree is descending order.
     */
    public Iterable<E> ascdes() {
        List<E> lista = new ArrayList<>();

        if (root == null) {
            return lista;
        }

        this.ascSubtree(root.getLeft(), lista);

        lista.add(root.getElement());

        this.desSubtree(root.getRight(), lista);

        return lista;
    }

    private void ascSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        ascSubtree(node.getLeft(), snapshot);
        snapshot.add(node.getElement());
        ascSubtree(node.getRight(), snapshot);
    }

    private void desSubtree(Node<E> node, List<E> snapshot) {
        if (node == null) {
            return;
        }
        desSubtree(node.getRight(), snapshot);
        snapshot.add(node.getElement());
        desSubtree(node.getLeft(), snapshot);
    }

}
