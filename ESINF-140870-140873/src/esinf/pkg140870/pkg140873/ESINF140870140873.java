package esinf.pkg140870.pkg140873;

import ProjectsManager.TREE;
import graph.Edge;
import graph.Graph;
import graph.Vertex;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import model.Activity;
import model.ActivityList;
import ProjectsManager.Files;
import ProjectsManager.Project;
import ProjectsManager.ProjectList;
import ProjectsManager.ProjectManager;
import java.util.Locale;
import model.PertCpm;

/**
 *
 * @author 1140870_1140873
 */
public class ESINF140870140873 {

    // private final static String FILE = "file.txt";
    private final static String FILEPROJECT = "project.txt";

    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        /*ActivityList activities = new ActivityList();

         Files file = new Files(activities);
         file.readFile(FILE);

         for (Activity activity : activities.getActivites()) {
         System.out.println(activity.toString());
         }

         PertCpm pertCpm = new PertCpm();
         Graph graph = pertCpm.getGraph();
         Iterator<Vertex<Activity, String>> itVerts;
         Iterator<Edge<Activity, String>> itEdge;

         pertCpm.insertVertex(activities.getActivites());
         pertCpm.insertEdges(activities.getActivites());

         itVerts = graph.vertices().iterator();
         itEdge = graph.edges().iterator();

         System.out.print("\n\nVerts: ");
         while (itVerts.hasNext()) {
         System.out.print(itVerts.next().getElement().getKey() + " ");
         }

         System.out.print("\n\nEdges: ");
         while (itEdge.hasNext()) {
         Edge<Activity, String> edge = itEdge.next();
         System.out.print(edge.getVOrig().getElement().getKey() + "-"
         + edge.getVDest().getElement().getKey() + "   ");

         }

         pertCpm.calcEarliest();

         HashMap<Vertex<Activity, String>, Integer> eFinish = new HashMap<>();

         System.out.println("\n\nEarliest Finish:");
         eFinish = pertCpm.getEarliestFinish();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : eFinish.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         HashMap<Vertex<Activity, String>, Integer> eStart = new HashMap<>();

         System.out.println("\n\nEarliest Start:");
         eStart = pertCpm.getEarliestStart();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : eStart.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         pertCpm.calcLatest();

         HashMap<Vertex<Activity, String>, Integer> lFinish = new HashMap<>();

         System.out.println("\n\nLatestFinish:");
         lFinish = pertCpm.getLatestFinish();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : lFinish.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         HashMap<Vertex<Activity, String>, Integer> lStart = new HashMap<>();

         System.out.println("\n\nLatestStart:");
         lStart = pertCpm.getLatestStart();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : lStart.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         pertCpm.calcSlack();

         HashMap<Vertex<Activity, String>, Integer> slack = new HashMap<>();

         System.out.println("\n\nSlack:");
         slack = pertCpm.getSlack();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : slack.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         Map<Vertex<Activity, String>, Integer> orderHMEF = new HashMap<>();

         System.out.println("\n\nActivities by its order:");
         orderHMEF = pertCpm.activityOrderCompletion();
         for (Map.Entry<Vertex<Activity, String>, Integer> es : orderHMEF.entrySet()) {
         System.out.println(es.getKey().getElement().getKey() + " " + es.getValue());
         }

         System.out.println("\n\nAll Paths Between the Start and Finish:");
         HashMap<Deque<Activity>, Integer> paths = new HashMap<>();
         paths = pertCpm.allPaths();

         for (Map.Entry<Deque<Activity>, Integer> p : paths.entrySet()) {
         System.out.println("-> Length:" + p.getValue());
         while (!p.getKey().isEmpty()) {
         System.out.print(" " + p.getKey().pop().getKey() + " ");
         }
         System.out.println("\n");
         }

         System.out.println("\n\nAll Critical Path:");
         ArrayList<Deque<Activity>> criticalPaths = new ArrayList<>();
         criticalPaths = pertCpm.criticalPath();
         for (int i = 0; i < criticalPaths.size(); i++) {
         while (!criticalPaths.get(i).isEmpty()) {
         System.out.print(" " + criticalPaths.get(i).pop().getKey() + " ");
         }
         System.out.println("\n");

         }*/
        ProjectsManager.ActivityList activities = new ProjectsManager.ActivityList();
        ProjectsManager.ProjectList projects = new ProjectsManager.ProjectList();

        ProjectsManager.Files file = new ProjectsManager.Files(activities, projects);
        file.readFile(FILEPROJECT);

        ProjectsManager.ProjectManager pm = new ProjectManager();
        pm.insertInProjectTree(projects.getProjects());
        pm.insertInActivityTree(activities.getActivites());
        pm.displayInOrder();

        ArrayList<ProjectsManager.Activity> array_activities = pm.getLatestActivity(projects.getProjects().get(0), projects.getProjects().get(3));
        if (array_activities != null) {
            for (ProjectsManager.Activity a : array_activities) {
                System.out.println(a);
            }
        }
    }
}
