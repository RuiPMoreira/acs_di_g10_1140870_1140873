package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class Activity {

    private String key;
    private String type;
    private String description;
    private int duration;
    private String durationUnit;
    private List<Activity> predecessors;

    /**
     * Construtor de uma instancia de Actitvity
     *
     */
    public Activity() {
        predecessors = new ArrayList<>();
    }

    /**
     * Construtor de uma instancia de Actitvity
     *
     *
     * @param key
     * @param type
     * @param description
     * @param duration
     * @param durationUnit
     */
    public Activity(String key, String type, String description, int duration,
            String durationUnit, List<Activity> predecessors) throws Exception {
        this.setKey(key);
        this.setType(type);
        this.setDescription(description);
        this.setDuration(duration);
        this.setDurationUnit(durationUnit);
        this.setPredecessors(predecessors);
    }

    /**
     * Construtor de uma instancia de Actitvity
     *
     * @param key
     * @param type
     * @param description
     * @param duration
     * @param durationUnit
     * @throws java.lang.Exception
     */
    public Activity(String key, String type, String description, int duration, String durationUnit) throws Exception {
        this.setKey(key);
        this.setType(type);
        this.setDescription(description);
        this.setDuration(duration);
        this.setDurationUnit(durationUnit);
        this.predecessors = new ArrayList<>();
    }

    /**
     * Construtor de uma instancia de Actitvity
     *
     */
    public Activity(Activity activity) throws Exception {
        this(activity.getKey(), activity.getType(), activity.getDescription(),
                activity.getDuration(), activity.getDurationUnit(), activity.getPredecessors());
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) throws Exception {
        if (type.equalsIgnoreCase("fca") || type.equalsIgnoreCase("vca")) {
            this.type = type;
        } else {
            throw new Exception("Type unknow");
        }

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public List<Activity> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(List<Activity> predecessors) {
        this.predecessors = predecessors;
    }

    @Override
    public String toString() {
        String str = getKey() + "," + getType() + "," + getDescription() + "," + getDuration() + "," + getDurationUnit();
        if (getPredecessors().size() > 0) {
            for (Activity activity : getPredecessors()) {
                str += "," + activity.getKey();
            }
        }
        return str;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Activity)) {
            return false;
        }
        Activity a = (Activity) obj;
        if (!this.getKey().contentEquals(a.getKey())) {
            return false;
        }
        return true;
    }
}
