package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityList {

    private final List<Activity> activites;

    public ActivityList() {
        activites = new ArrayList<>();
    }

    public Activity getActivityByKey(String key) {
        for (Activity activity : activites) {
            if (activity.getKey().equalsIgnoreCase(key.trim())) {
                return activity;
            }
        }
        return null;
    }

    public List<Activity> getActivites() {
        return activites;
    }

    public void add(Activity a) {
        activites.add(a);
    }

}
