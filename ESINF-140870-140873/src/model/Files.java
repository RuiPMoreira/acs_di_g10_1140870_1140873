package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author 1140870_1140873
 */
public class Files {

    private final ActivityList activities;

    public Files(ActivityList activities) {
        this.activities = activities;
    }

    public void readFile(String file) throws Exception {
        int i = 1;
        try (Scanner scanner = new Scanner(new File(file), "UTF-8")) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (line.length() > 0) {
                    String[] fields = line.split(",");
                    if (fields.length > 1) {
                        if (fields[1].trim().equalsIgnoreCase("vca")) {
                            if (fields.length >= 7) {
                                variableActivity(fields, i);
                            } else {
                                System.err.println("Campos obrigatorios em falta na linha " + i);
                            }
                        } else if (fields[1].trim().equalsIgnoreCase("fca")) {
                            if (fields.length >= 6) {
                                fixedCoastActivity(fields, i);
                            } else {
                                System.err.println("Campos obrigatorios em falta na linha " + i);
                            }
                        } else {
                            System.err.println("O campo 'type' na linha " + i + " é invalido.");
                        }
                    }
                }
                i++;
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Não Existe Ficheiro.");
        }
    }

    private void variableActivity(String[] vec, int j) throws Exception {
        try {
            Activity activity = new Activity(vec[0], vec[1], vec[2], Integer.parseInt(vec[3]), vec[4]);
            if (vec.length > 6) {
                for (int i = 7; i < vec.length; i++) {
                    Activity predecessors = activities.getActivityByKey(vec[i]);
                    if (predecessors != null) {
                        activity.getPredecessors().add(predecessors);
                    }
                }
            }
            VariableCostActivity vca = new VariableCostActivity(activity, Integer.parseInt(vec[5]), Integer.parseInt(vec[6]));
            activities.add(vca);
        } catch (NumberFormatException ex) {
            System.err.println("Existe um erro na linha " + j);
        }
    }

    private void fixedCoastActivity(String[] vec, int j) throws Exception {
        try {
            Activity activity = new Activity(vec[0], vec[1], vec[2], Integer.parseInt(vec[3]), vec[4]);
            if (vec.length > 5) {
                for (int i = 6; i < vec.length; i++) {
                    Activity predecessors = activities.getActivityByKey(vec[i]);
                    if (predecessors != null) {
                        activity.getPredecessors().add(predecessors);
                    }
                }
            }
            FixedCoastActivity fca = new FixedCoastActivity(activity, Integer.parseInt(vec[5]));
            activities.add(fca);
        } catch (NumberFormatException ex) {
            System.err.println("Existe um erro na linha " + j);
        }
    }
}
