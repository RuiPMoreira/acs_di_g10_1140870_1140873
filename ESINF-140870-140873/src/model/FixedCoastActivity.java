package model;

import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class FixedCoastActivity extends Activity {

    private int totalCost;

    public FixedCoastActivity(String key, String type, String description, int duration, String durationUnit, int totalCost) throws Exception {
        super(key, type, description, duration, durationUnit);
        this.totalCost = totalCost;
    }

    public FixedCoastActivity(String key, String type, String description, int duration, String durationUnit, int totalCost, List<Activity> predecessors) throws Exception {
        super(key, type, description, duration, durationUnit, predecessors);
        this.totalCost = totalCost;

    }

    public FixedCoastActivity(Activity activity, int totalCost) throws Exception {
        super(activity);
        this.totalCost = totalCost;
    }

    public int getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(int totalCost) {
        this.totalCost = totalCost;
    }

}
