package model;

import graph.Edge;
import graph.Graph;
import graph.GraphAlgorithms;
import graph.Vertex;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1140870_1140873
 */
public class PertCpm {

    private Graph<Activity, String> graph;
    private Activity start;
    private Activity finish;
    private HashMap<Vertex<Activity, String>, Integer> hmES;
    private HashMap<Vertex<Activity, String>, Integer> hmEF;
    private HashMap<Vertex<Activity, String>, Integer> hmLF;
    private HashMap<Vertex<Activity, String>, Integer> hmLS;
    private HashMap<Vertex<Activity, String>, Integer> hmSlack;

    /**
     * Construtor de uma instância de PertCmp
     */
    public PertCpm() {
        graph = new Graph<>(true);
        try {
            this.start = new Activity("start", "fca", "fixStart", 0, "week");
            this.finish = new Activity("finish", "fca", "fixFinish", 0, "week");
            hmEF = new HashMap<>();
            hmES = new HashMap<>();
            hmLF = new HashMap<>();
            hmLS = new HashMap<>();
            hmSlack = new HashMap<>();
        } catch (Exception ex) {
            Logger.getLogger(PertCpm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Método que devolve o grafo PERT
     *
     * @return
     */
    public Graph getGraph() {
        return graph;
    }

    /**
     * Método que devolve o Earliest Start do grafo
     *
     * @return
     */
    public HashMap<Vertex<Activity, String>, Integer> getEarliestStart() {
        return hmES;
    }

    /**
     * Método que devolve o Earliest Finish do grafo
     *
     * @return
     */
    public HashMap<Vertex<Activity, String>, Integer> getEarliestFinish() {
        return hmEF;
    }

    /**
     * Método que devolve o Latest Start do grafo
     *
     * @return
     */
    public HashMap<Vertex<Activity, String>, Integer> getLatestStart() {
        return hmLS;
    }

    /**
     * Método que devolve o Latest Finish do grafo
     *
     * @return
     */
    public HashMap<Vertex<Activity, String>, Integer> getLatestFinish() {
        return hmLF;
    }

    /**
     * Método que devolve o slack do grafo
     *
     * @return
     */
    public HashMap<Vertex<Activity, String>, Integer> getSlack() {
        return hmSlack;
    }

    /**
     * Método que insere as Actividades no grafo
     *
     * @param listActivities
     */
    public void insertVertex(List<Activity> listActivities) {
        graph.insertVertex(start);
        for (Activity activity : listActivities) {
            graph.insertVertex(activity);
        }
        graph.insertVertex(finish);
    }

    /**
     * Método que insere os edges para o grafo
     *
     * @param listActivities
     */
    public void insertEdges(List<Activity> listActivities) {
        for (Activity activity : listActivities) {
            if (activity.getPredecessors().isEmpty()) {
                graph.insertEdge(start, activity, "edge", 1);
            } else {
                for (Activity predecessors : activity.getPredecessors()) {
                    graph.insertEdge(predecessors, activity, "edge", 1);
                }
            }
        }
        Iterator<Vertex<Activity, String>> itVerts;
        itVerts = graph.vertices().iterator();
        while (itVerts.hasNext()) {
            Vertex<Activity, String> vertex = itVerts.next();
            if (graph.outDegree(vertex) == 0 && !vertex.getElement().equals(finish)) {
                graph.insertEdge(vertex.getElement(), finish, null, 0);
            }
        }
        if (cycles()) {
            Iterator<Edge<Activity, String>> itEdge;
            itEdge = graph.edges().iterator();
            while (itEdge.hasNext()) {
                Edge<Activity, String> edge = itEdge.next();
                graph.removeEdge(edge);
            }
        }

    }

    /**
     * Método que calcula o Earliest Start e Earlist Finish do grafo
     */
    public void calcEarliest() {
        Iterator<Edge<Activity, String>> itEdge;
        itEdge = graph.edges().iterator();
        int sum;
        while (itEdge.hasNext()) {
            Edge<Activity, String> edge = itEdge.next();
            if (edge.getVOrig().getElement().equals(start)) {
                hmES.put(edge.getVDest(), 0);
                hmEF.put(edge.getVDest(), edge.getVDest().getElement().getDuration());
            } else {
                if (!edge.getVDest().getElement().equals(finish)) {
                    sum = 0;
                    for (Activity activity : edge.getVDest().getElement().getPredecessors()) {
                        for (Entry<Vertex<Activity, String>, Integer> es : hmEF.entrySet()) {
                            if (activity.equals(es.getKey().getElement())) {
                                if (es.getValue() > sum) {
                                    sum = es.getValue();
                                }
                            }
                        }
                    }
                    hmES.put(edge.getVDest(), sum);

                    sum = 0;
                    sum += edge.getVDest().getElement().getDuration();
                    for (Entry<Vertex<Activity, String>, Integer> es : hmES.entrySet()) {
                        if (edge.getVDest().getElement().equals(es.getKey().getElement())) {
                            sum += es.getValue();
                        }
                    }
                    hmEF.put(edge.getVDest(), sum);
                }
            }
        }
    }

    /**
     * Método que recebe um HashMap do Earliest Finish e devolve o maior deles
     *
     * @param hmEF
     * @return
     */
    private int largerEF(HashMap<Vertex<Activity, String>, Integer> hmEF) {
        int larger = 0;
        for (Map.Entry<Vertex<Activity, String>, Integer> ef : hmEF.entrySet()) {
            if (larger < ef.getValue()) {
                larger = ef.getValue();
            }
        }
        return larger;
    }

    /**
     * Método que calcula o Latest Finish do grafo
     */
    private void calcLatestFinish() {
        Iterator<Edge<Activity, String>> itEdge;
        itEdge = graph.edges().iterator();
        ArrayList<Edge<Activity, String>> alEdge = new ArrayList<>();
        while (itEdge.hasNext()) {
            alEdge.add(itEdge.next());
        }
        int larger = largerEF(hmEF);
        for (int i = alEdge.size() - 1; i >= 0; i--) {
            Edge<Activity, String> edge = alEdge.get(i);
            if (edge.getVDest().getElement().equals(finish)) {
                for (Map.Entry<Vertex<Activity, String>, Integer> ef : hmEF.entrySet()) {
                    if (edge.getVOrig().getElement().equals(ef.getKey().getElement())) {
                        hmLF.put(edge.getVOrig(), larger);
                        for (Activity activity : edge.getVOrig().getElement().getPredecessors()) {
                            int min = (ef.getValue() - edge.getVOrig().getElement().getDuration());
                            hmLF.put(graph.getVertex(activity), min);
                        }
                    }
                }
            } else {
                for (Activity activity : edge.getVDest().getElement().getPredecessors()) {
                    int x = 0;
                    for (Entry<Vertex<Activity, String>, Integer> lf : hmLF.entrySet()) {
                        if (edge.getVDest().getElement().equals(lf.getKey().getElement())) {
                            x = lf.getValue();
                            break;
                        }
                    }
                    if (x != 0) {
                        boolean ex = false;
                        int y = 0;
                        for (Entry<Vertex<Activity, String>, Integer> lf : hmLF.entrySet()) {

                            if (activity.equals(lf.getKey().getElement())) {
                                ex = true;
                                y = lf.getValue();
                                break;
                            }
                        }
                        if (ex == false) {
                            hmLF.put(graph.getVertex(activity), (x - edge.getVDest().getElement().getDuration()));
                        } else {
                            if (y > (x - edge.getVDest().getElement().getDuration())) {
                                hmLF.put(graph.getVertex(activity), (x - edge.getVDest().getElement().getDuration()));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Método que calcula o Latest Start do grafo
     */
    private void calcLatestStart() {
        Iterator<Vertex<Activity, String>> itVertex;
        itVertex = graph.vertices().iterator();
        while (itVertex.hasNext()) {
            Vertex<Activity, String> vertex = itVertex.next();
            for (Entry<Vertex<Activity, String>, Integer> lf : hmLF.entrySet()) {
                if (vertex.getElement().equals(lf.getKey().getElement())) {
                    hmLS.put(vertex, (lf.getValue() - vertex.getElement().getDuration()));
                }
            }
        }
    }

    /**
     * Método que calcula o Latest Start e o Latest Finish do grafo
     */
    public void calcLatest() {
        calcLatestFinish();
        calcLatestStart();
    }

    /**
     * Metodo que calcula o slack do grafo
     */
    public void calcSlack() {
        for (Entry<Vertex<Activity, String>, Integer> ef : hmEF.entrySet()) {
            for (Entry<Vertex<Activity, String>, Integer> lf : hmLF.entrySet()) {
                if (ef.getKey().getElement().equals(lf.getKey().getElement())) {
                    hmSlack.put(ef.getKey(), (lf.getValue() - ef.getValue()));
                }
            }
        }
    }

    /**
     * Metodo que devolve as atividades por ordem de conclusão
     *
     * @return Map das actividades
     */
    public Map activityOrderCompletion() {
        List list = new LinkedList(hmEF.entrySet());

        Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });

        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    /**
     * Método que devolve um hashMap com todos os caminhos possiveis e o custo
     * total
     *
     * @return HasMap com os caminhos e o custo
     */
    public HashMap<Deque<Activity>, Integer> allPaths() {
        HashMap<Deque<Activity>, Integer> paths = new HashMap();
        ArrayList<Deque<Activity>> oldPaths = GraphAlgorithms.allPaths(graph, graph.getVertex(start).getElement(), graph.getVertex(finish).getElement());

        for (Deque<Activity> p : oldPaths) {
            int duration = 0;
            for (Activity a : p) {
                duration += a.getDuration();
            }
            paths.put(p, duration);
        }

        return paths;
    }

    /**
     * Método que devolve todos os caminhos criticos do grafo
     *
     * @return ArrayList com os caminhos
     */
    public ArrayList<Deque<Activity>> criticalPath() {
        ArrayList<Deque<Activity>> criticalPath = new ArrayList<>();
        HashMap<Deque<Activity>, Integer> paths = new HashMap<>();
        paths = allPaths();
        Deque<Activity> tmpPaths;

        for (Map.Entry<Deque<Activity>, Integer> p : paths.entrySet()) {
            tmpPaths = new LinkedList<>(p.getKey());
            boolean bool = false;
            while (!tmpPaths.isEmpty()) {
                Activity t = tmpPaths.pop();
                for (Map.Entry<Vertex<Activity, String>, Integer> es : hmSlack.entrySet()) {
                    if (t.equals(es.getKey().getElement()) && es.getValue() != 0) {
                        bool = true;
                        tmpPaths.clear();
                        break;
                    }
                }
            }
            if (bool == false) {
                criticalPath.add(p.getKey());
            }
        }
        return criticalPath;
    }

    /**
     * Método que verifica se o grafo pode ser PERT graph ou não
     *
     * @return true se o grafo tiver ciclos
     */
    private boolean cycles() {
        Boolean bool = false;
        Iterator<Edge<Activity, String>> itEdge;
        itEdge = graph.edges().iterator();
        while (itEdge.hasNext()) {
            Edge<Activity, String> edge = itEdge.next();
            while (itEdge.hasNext()) {
                Edge<Activity, String> edgeNext = itEdge.next();
                if (edge.getVOrig().getElement().equals(edgeNext.getVDest().getElement())) {
                    bool = true;
                    break;
                }
            }
            if (bool) {
                break;
            }
        }
        return bool;
    }

}
