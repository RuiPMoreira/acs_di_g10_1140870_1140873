package model;

import java.util.List;

/**
 *
 * @author 1140870_1140873
 */
public class VariableCostActivity extends Activity {

    private int costTime;
    private int totalTime;

    public VariableCostActivity(String key, String type, String description,
            int duration, String durationUnit, int costTime, int totalTime, List<Activity> predecessors) throws Exception {
        super(key, type, description, duration, durationUnit, predecessors);
        this.costTime = costTime;
        this.totalTime = totalTime;
    }

    public VariableCostActivity(String key, String type, String description, int duration, String durationUnit, int costTime, int totalTime) throws Exception {
        super(key, type, description, duration, durationUnit);
        this.costTime = costTime;
        this.totalTime = totalTime;
    }

    public VariableCostActivity(Activity activity, int costTime, int totalTime) throws Exception {
        super(activity);
        this.costTime = costTime;
        this.totalTime = totalTime;
    }

    public int getCostTime() {
        return costTime;
    }

    public void setCostTime(int costTime) {
        this.costTime = costTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

}
