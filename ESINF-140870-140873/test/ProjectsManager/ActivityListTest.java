package ProjectsManager;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityListTest {
    private ActivityList activities;
    public ActivityListTest() {
        activities = new ActivityList();
        activities.add(new Activity("Project1", "A", "Type1", 20, 10));
        activities.add(new Activity("Project2", "A", "Type1", 20, 10));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActivites method, of class ActivityList.
     */
    @Test
    public void testGetActivites() {
        System.out.println("getActivites");
        ActivityList instance = activities;
        List<Activity> expResult = new ArrayList<>();
        expResult.add(new Activity("Project1", "A", "Type1", 20, 10));
        expResult.add(new Activity("Project2", "A", "Type1", 20, 10));
        List<Activity> result = instance.getActivites();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class ActivityList.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Activity a = new Activity("Project1", "B", "Type1", 20, 10);
        ActivityList instance = new ActivityList();
        instance.add(a);
    }
    
}
