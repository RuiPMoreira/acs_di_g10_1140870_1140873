package ProjectsManager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityTest {
    private Activity a2;
    public ActivityTest() {
        a2 = new Activity("Project1", "A", "Type1", 20, 5);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectRef method, of class Activity.
     */
    @Test
    public void testGetProjectRef() {
        System.out.println("getProjectRef");
        Activity instance = a2;
        String expResult = "Project1";
        String result = instance.getProjectRef();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProjectRef method, of class Activity.
     */
    @Test
    public void testSetProjectRef() {
        System.out.println("setProjectRef");
        String projectRef = "Project2";
        Activity instance = a2;
        instance.setProjectRef(projectRef);
    }

    /**
     * Test of getCode method, of class Activity.
     */
    @Test
    public void testGetCode() {
        System.out.println("getCode");
        Activity instance = a2;
        String expResult = "A";
        String result = instance.getCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCode method, of class Activity.
     */
    @Test
    public void testSetCode() {
        System.out.println("setCode");
        String code = "B";
        Activity instance = a2;
        instance.setCode(code);
    }

    /**
     * Test of getType method, of class Activity.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Activity instance = a2;
        String expResult = "Type1";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setType method, of class Activity.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "Type2";
        Activity instance = a2;
        instance.setType(type);
    }

    /**
     * Test of getDurationTime method, of class Activity.
     */
    @Test
    public void testGetDurationTime() {
        System.out.println("getDurationTime");
        Activity instance = a2;
        int expResult = 20;
        int result = instance.getDurationTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDurationTime method, of class Activity.
     */
    @Test
    public void testSetDurationTime() {
        System.out.println("setDurationTime");
        int durationTime = 30;
        Activity instance = a2;
        instance.setDurationTime(durationTime);
    }

    /**
     * Test of getDelayTime method, of class Activity.
     */
    @Test
    public void testGetDelayTime() {
        System.out.println("getDelayTime");
        Activity instance = a2;
        int expResult = 5;
        int result = instance.getDelayTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDelayTime method, of class Activity.
     */
    @Test
    public void testSetDelayTime() {
        System.out.println("setDelayTime");
        int delayTime = 10;
        Activity instance = a2;
        instance.setDelayTime(delayTime);
    }

    /**
     * Test of compareTo method, of class Activity.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object o = new Activity("Project1", "A", "Type1", 10, 4);
        Activity instance = a2;
        int expResult = 1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Activity.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Activity("Project1", "A", "Type1", 20, 5);
        Activity instance = a2;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Activity.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Activity instance = a2;
        String expResult = "Activity ->  Project Reference=Project1, Code=A, Type=Type1, Duration Time=20, Delay Time=5";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
