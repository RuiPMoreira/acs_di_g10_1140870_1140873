package ProjectsManager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class FilesTest {
    
    private ProjectList projects;
    private ActivityList activities;
    
    public FilesTest() {
        projects = new ProjectList();
        activities = new ActivityList();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Teste de Sucesso
     */
    @Test
    public void testReadFile() throws Exception {
        System.out.println("readFile");
        String file = "test/testFileP.txt";
        Files instance = new Files(activities, projects);
        instance.readFile(file);
    }
    
    /**
     * Ficheiro Inexistente
     */
    @Test
    public void testReadFile2() throws Exception {
        System.out.println("readFile");
        String file = "test/testFileP100.txt";
        Files instance = new Files(activities, projects);
        instance.readFile(file);
    }
    
    /**
     * Ficheiro contem uma linha cuja referencia de projecto se destina a algo inexistente.
     * O comportamento esperado será ler o ficheiro todo e ignorar as linhas invalidas.
     */
    @Test
    public void testReadFile3() throws Exception {
        System.out.println("readFile");
        String file = "test/testFileP2.txt";
        Files instance = new Files(activities, projects);
        instance.readFile(file);
    }
    
    /**
     * Ficheiro contem 2 erros, o primeiro é que o somatorio das durações das atividades não vai corresnder ao total do projecto.
     * O segundo erro corresponde ao somatorio do delay das actividades de um determinado projecto que não irá corresnponder ao delay do projecto.
     */
    @Test
    public void testReadFile4() throws Exception {
        System.out.println("readFile");
        String file = "test/testFileP3.txt";
        Files instance = new Files(activities, projects);
        instance.readFile(file);
    }
    
}
