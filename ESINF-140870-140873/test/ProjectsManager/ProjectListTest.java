package ProjectsManager;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ProjectListTest {
    private ProjectList projects;
    public ProjectListTest() {
        projects = new ProjectList();
        projects.add(new Project("Project1", "A", 20, 2));
        projects.add(new Project("Project2", "A", 30, 3));
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getProjects method, of class ProjectList.
     */
    @Test
    public void testGetProjects() {
        System.out.println("getProjects");
        ProjectList instance = projects;
        List<Project> expResult = new ArrayList<>();
        expResult.add(new Project("Project1", "A", 20, 2));
        expResult.add(new Project("Project2", "A", 30, 3));
        List<Project> result = instance.getProjects();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class ProjectList.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Project p = new Project("Project4", "A", 40, 4);
        ProjectList instance = new ProjectList();
        instance.add(p);
    }
    
}
