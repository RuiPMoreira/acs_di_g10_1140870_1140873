package ProjectsManager;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ProjectManagerTest {
    private ProjectManager pm;
    private ProjectList projects;
    private ActivityList activities;
    public ProjectManagerTest() {
        pm = new ProjectManager();
        
        
        projects = new ProjectList();
        projects.add(new Project("Project1", "A", 20, 5));
        projects.add(new Project("Project2", "B", 30, 3));
        
        
        activities = new ActivityList();
        activities.add(new Activity("Project1", "A", "Type1", 10, 3));
        activities.add(new Activity("Project2", "A", "Type1", 30, 3));
        activities.add(new Activity("Project3", "b", "Type2", 10, 2));
        
        pm.insertInProjectTree(projects.getProjects());
        pm.insertInActivityTree(activities.getActivites());
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertInProjectTree method, of class ProjectManager.
     */
    @Test
    public void testInsertInProjectTree() {
        System.out.println("insertInProjectTree");
        List<Project> projectos = projects.getProjects();
        projectos.add(new Project("Project3", "B", 10, 2));
        ProjectManager instance = new ProjectManager();
        instance.insertInProjectTree(projectos);
    }

    /**
     * Test of insertInActivityTree method, of class ProjectManager.
     */
    @Test
    public void testInsertInActivityTree() {
        System.out.println("insertInActivityTree");
        List<Activity> atividades = new ArrayList<>(activities.getActivites());
        atividades.add(new Activity("Project4", "B", "Type2", 30, 10));
        ProjectManager instance = new ProjectManager();
        instance.insertInActivityTree(atividades);
    }

    /**
     * Test of displayInOrder method, of class ProjectManager.
     */
    @Test
    public void testDisplayInOrder() {
        System.out.println("displayInOrder");
        ProjectManager instance = pm;
        instance.displayInOrder();
    }

    /**
     * Test of getLatestActivity method, of class ProjectManager.
     */
    @Test
    public void testGetLatestActivity() {
        System.out.println("getLatestActivity");
        Project projecto1 = new Project("Project1", "A", 20, 2);
        Project projecto2 = new Project("Project2", "A", 30, 3);
        ProjectManager instance = pm;
        ArrayList<Activity> expResult = new ArrayList<>();
        expResult.add(new Activity("Project1", "A", "Type1", 10, 3));
        expResult.add(new Activity("Project2", "A", "Type1", 30, 3));
        ArrayList<Activity> result = instance.getLatestActivity(projecto1, projecto2);
        assertEquals(expResult, result);
    }
    
}
