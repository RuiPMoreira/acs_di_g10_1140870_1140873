package ProjectsManager;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ProjectTest {
    private Project project;
    public ProjectTest() {
        project = new Project("Project1", "A", 10, 2);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getReference method, of class Project.
     */
    @Test
    public void testGetReference() {
        System.out.println("getReference");
        Project instance = project;
        String expResult = "Project1";
        String result = instance.getReference();
        assertEquals(expResult, result);
    }

    /**
     * Test of setReference method, of class Project.
     */
    @Test
    public void testSetReference() {
        System.out.println("setReference");
        String reference = "Project2";
        Project instance = project;
        instance.setReference(reference);
    }

    /**
     * Test of getType method, of class Project.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Project instance = project;
        String expResult = "A";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setType method, of class Project.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "B";
        Project instance = project;
        instance.setType(type);
    }

    /**
     * Test of getCompletionTime method, of class Project.
     */
    @Test
    public void testGetCompletionTime() {
        System.out.println("getCompletionTime");
        Project instance = project;
        int expResult = 10;
        int result = instance.getCompletionTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCompletionTime method, of class Project.
     */
    @Test
    public void testSetCompletionTime() {
        System.out.println("setCompletionTime");
        int completionTime = 50;
        Project instance = project;
        instance.setCompletionTime(completionTime);
    }

    /**
     * Test of getDelayTime method, of class Project.
     */
    @Test
    public void testGetDelayTime() {
        System.out.println("getDelayTime");
        Project instance = project;
        int expResult = 2;
        int result = instance.getDelayTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDelayTime method, of class Project.
     */
    @Test
    public void testSetDelayTime() {
        System.out.println("setDelayTime");
        int delayTime = 20;
        Project instance = project;
        instance.setDelayTime(delayTime);
    }

    /**
     * Test of compareTo method, of class Project.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Object o = new Project("Project1", "A", 10, 3);
        Project instance = project;
        int expResult = -1;
        int result = instance.compareTo(o);
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Project.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = new Project("Project1", "A", 10, 2);
        Project instance = project;
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Project.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Project instance = project;
        String expResult = "Project ->  Reference=Project1, Type=A, Completion Time=10, Delay=2";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
