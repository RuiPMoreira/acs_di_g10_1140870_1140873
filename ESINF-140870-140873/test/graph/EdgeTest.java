/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rui Moreira
 */
public class EdgeTest {
    
    public EdgeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getElement method, of class Edge.
     */
    @Test
    public void testGetElement() {
        System.out.println("getElement");
        Edge instance = new Edge();
        Object expResult = instance;
        instance.setElement(expResult);
        Object result = instance.getElement();
        assertEquals(expResult, result);
    }

    /**
     * Test of setElement method, of class Edge.
     */
    @Test
    public void testSetElement() {
        System.out.println("setElement");
        Edge eInf = new Edge();
        Edge instance = new Edge();
        instance.setElement(eInf);
    }

    /**
     * Test of getWeight method, of class Edge.
     */
    @Test
    public void testGetWeight() {
        System.out.println("getWeight");
        Edge instance = new Edge();
        double expResult = 4.0;
        instance.setWeight(4.0);
        double result = instance.getWeight();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setWeight method, of class Edge.
     */
    @Test
    public void testSetWeight() {
        System.out.println("setWeight");
        double ew = 4.0;
        Edge instance = new Edge();
        instance.setWeight(ew);
    }

    /**
     * Test of getVOrig method, of class Edge.
     */
    @Test
    public void testGetVOrig() {
        System.out.println("getVOrig");
        Edge instance = new Edge();
        Vertex expResult = new Vertex(0, instance);
        instance.setVOrig(expResult);
        Vertex result = instance.getVOrig();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVOrig method, of class Edge.
     */
    @Test
    public void testSetVOrig() {
        System.out.println("setVOrig");
        Edge instance = new Edge();
        Vertex expResult = new Vertex(0, instance);
        instance.setVOrig(expResult);
    }

    /**
     * Test of getVDest method, of class Edge.
     */
    @Test
    public void testGetVDest() {
        System.out.println("getVDest");
        Edge instance = new Edge();
        Vertex expResult = new Vertex(0, instance);
        instance.setVDest(expResult);
        Vertex result = instance.getVDest();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setVDest method, of class Edge.
     */
    @Test
    public void testSetVDest() {
        System.out.println("setVDest");
        Edge instance = new Edge();
        Vertex expResult = new Vertex(0, instance);
        instance.setVDest(expResult);
    }

    /**
     * Test of getEndpoints method, of class Edge.
     */
    @Test
    public void testGetEndpoints() {
        System.out.println("getEndpoints");
        Edge instance = new Edge();
        Vertex[] expResult = new Vertex[2];
        
        Edge e1 = new Edge();
        Vertex v1 = new Vertex(0, e1);
        Edge e2 = new Edge();
        Vertex v2 = new Vertex(0, e2);
        
        expResult[0] = v1;
        expResult[1] = v2;
        
        instance.setVOrig(v1);
        instance.setVDest(v2);
        
        Vertex[] result = instance.getEndpoints();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Edge.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Edge otherObj = new Edge();
        Edge instance = otherObj;
        boolean expResult = true;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }

    /**
     * Test of compareTo method, of class Edge.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Edge otherObject = new Edge();
        otherObject.setWeight(4.0);
        Edge instance = new Edge();
        instance.setWeight(4.0);
        int expResult = 0;
        int result = instance.compareTo(otherObject);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Edge.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Edge instance = new Edge();
        instance.setWeight(4.0);
        instance.setVDest(new Vertex());
        String expResult = "	- 4.0 - null";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
