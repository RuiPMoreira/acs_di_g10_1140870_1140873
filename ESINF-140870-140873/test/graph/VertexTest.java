/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph;

import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rui Moreira
 */
public class VertexTest {
    
    public VertexTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getKey method, of class Vertex.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");
        Vertex instance = new Vertex();
        instance.setKey(1);
        int expResult = 1;
        int result = instance.getKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setKey method, of class Vertex.
     */
    @Test
    public void testSetKey() {
        System.out.println("setKey");
        int k = 2;
        Vertex instance = new Vertex();
        instance.setKey(k);
    }

    /**
     * Test of getElement method, of class Vertex.
     */
    @Test
    public void testGetElement() {
        System.out.println("getElement");
        Vertex instance = new Vertex();
        Edge expResult = new Edge();
        instance.setElement(expResult);
        Object result = instance.getElement();
        assertEquals(expResult, result);
    }

    /**
     * Test of setElement method, of class Vertex.
     */
    @Test
    public void testSetElement() {
        System.out.println("setElement");
        Edge expResult = new Edge();
        Vertex instance = new Vertex();
        instance.setElement(expResult);
    }

    /**
     * Test of getOutgoing method, of class Vertex.
     */
    @Test
    public void testGetOutgoing() {
        System.out.println("getOutgoing");
        Vertex instance = new Vertex(0, new Edge<String, String>("x", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        Map<Vertex<String, Integer>, Edge<String, Integer>> expResult = new HashMap<>();
        Map<Vertex<String, Integer>, Edge<String, Integer>> result = instance.getOutgoing();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Vertex.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Vertex otherObj = new Vertex(0, new Edge<String, String>("x", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        Vertex instance = new Vertex(0, new Edge<String, String>("x", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        boolean expResult = true;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class Vertex.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        Vertex instance = new Vertex(0, new Edge<String, String>("A", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        Vertex expResult = new Vertex(0, new Edge<String, String>("A", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        Vertex result = instance.clone();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Vertex.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Vertex instance = new Vertex(0, new Edge<String, String>("A", 4.0, new Vertex<String, String>(), new Vertex<String, String>()));
        String expResult = "	- (A)4.0 - null (0): ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
