package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityListTest {
    
    public ActivityListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getActivityByKey method, of class ActivityList.
     */
    @Test
    public void testGetActivityByKey() throws Exception {
        System.out.println("getActivityByKey");
        String key = "A";
        ActivityList instance = new ActivityList();
        instance.add(new Activity("A", "VCA", "Descrição", 3, "weeks"));
        Activity expResult = new Activity("A", "VCA", "Descrição", 3, "weeks");
        Activity result = instance.getActivityByKey(key);
        assertEquals(expResult, result);
    }

    /**
     * Test of getActivites method, of class ActivityList.
     */
    @Test
    public void testGetActivites() throws Exception {
        System.out.println("getActivites");
        ActivityList instance = new ActivityList();
        instance.add(new Activity("A", "VCA", "Descrição", 3, "weeks"));
        List<Activity> expResult = new ArrayList<>();
        expResult.add(new Activity("A", "VCA", "Descrição", 3, "weeks"));
        List<Activity> result = instance.getActivites();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class ActivityList.
     */
    @Test
    public void testAdd() throws Exception {
        System.out.println("add");
        Activity a = new Activity("A", "VCA", "Descrição", 3, "weeks");
        ActivityList instance = new ActivityList();
        instance.add(a);
    }
    
}
