package model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class ActivityTest {
    
    public ActivityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getKey method, of class Activity.
     */
    @Test
    public void testGetKey() {
        System.out.println("getKey");
        Activity instance = new Activity();
        instance.setKey("A");
        String expResult = "A";
        String result = instance.getKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setKey method, of class Activity.
     */
    @Test
    public void testSetKey() {
        System.out.println("setKey");
        String key = "A";
        Activity instance = new Activity();
        instance.setKey(key);
    }

    /**
     * Test of getType method, of class Activity.
     */
    @Test
    public void testGetType() throws Exception {
        System.out.println("getType");
        Activity instance = new Activity();
        instance.setType("vca");
        String expResult = "vca";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setType method, of class Activity.
     */
    @Test
    public void testSetType() throws Exception {
        System.out.println("setType");
        String type = "VCA";
        Activity instance = new Activity();
        instance.setType(type);
    }

    /**
     * Test of getDescription method, of class Activity.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Activity instance = new Activity();
        instance.setDescription("Description1");
        String expResult = "Description1";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescription method, of class Activity.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "Description1";
        Activity instance = new Activity();
        instance.setDescription(description);
    }

    /**
     * Test of getDuration method, of class Activity.
     */
    @Test
    public void testGetDuration() {
        System.out.println("getDuration");
        Activity instance = new Activity();
        instance.setDuration(2);
        int expResult = 2;
        int result = instance.getDuration();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDuration method, of class Activity.
     */
    @Test
    public void testSetDuration() {
        System.out.println("setDuration");
        int duration = 3;
        Activity instance = new Activity();
        instance.setDuration(duration);
    }

    /**
     * Test of getDurationUnit method, of class Activity.
     */
    @Test
    public void testGetDurationUnit() {
        System.out.println("getDurationUnit");
        Activity instance = new Activity();
        instance.setDurationUnit("weeks");
        String expResult = "weeks";
        String result = instance.getDurationUnit();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDurationUnit method, of class Activity.
     */
    @Test
    public void testSetDurationUnit() {
        System.out.println("setDurationUnit");
        String durationUnit = "week";
        Activity instance = new Activity();
        instance.setDurationUnit(durationUnit);
    }

    /**
     * Test of getPredecessors method, of class Activity.
     */
    @Test
    public void testGetPredecessors() throws Exception {
        System.out.println("getPredecessors");
        List<Activity> predecessors = new ArrayList<>();
        predecessors.add(new Activity("A", "VCA", "Descrição", 3, "weeks"));
        Activity instance = new Activity("A", "VCA", "Descrição", 3, "weeks", predecessors);
        List<Activity> expResult = new ArrayList<>();
        expResult.add(new Activity("A", "VCA", "Descrição", 3, "weeks"));
        List<Activity> result = instance.getPredecessors();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPredecessors method, of class Activity.
     */
    @Test
    public void testSetPredecessors() {
        System.out.println("setPredecessors");
        List<Activity> predecessors = new ArrayList<>();
        predecessors.add(new Activity());
        Activity instance = new Activity();
        instance.setPredecessors(predecessors);
    }

    /**
     * Test of toString method, of class Activity.
     */
    @Test
    public void testToString() throws Exception {
        System.out.println("toString");
        Activity instance = new Activity("A", "VCA", "Descrição", 3, "weeks");
        String expResult = "A,VCA,Descrição,3,weeks";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Activity.
     */
    @Test
    public void testEquals() throws Exception {
        System.out.println("equals");
        Object obj = new Activity("A", "VCA", "Descrição", 3, "weeks");
        Activity instance = new Activity("A", "VCA", "Descrição", 3, "weeks");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
