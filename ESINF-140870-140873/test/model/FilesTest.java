package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Fazer testes individuais para conseguir obter as mensagens de erro pela ordem
 * correcta.
 *
 *
 * @author 1140870_1140873
 */
public class FilesTest {

    public FilesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Nao irá aparecer nenhuma mensagem indicativa de erros
     */
    @Test
    public void testReadFile() throws Exception {
        System.out.println();
        System.out.println("Caso de Sucesso");
        Files instance = new Files(new ActivityList());
        instance.readFile("test/testFile.txt");
    }

    /**
     * Ficheiro não será encontrado.
     */
    @Test
    public void testReadFile2() throws Exception {
        System.out.println();
        System.out.println("\nFalha ao encontrar ficheiro:");
        Files instance = new Files(new ActivityList());
        instance.readFile("test/noFile.txt");
    }

    /**
     * Irá surgir uma mensagem de erro para as linhas cujo o numero de campos
     * seja inferior ao exigido, no entanto o erro não irá terminar o programa,
     * a linha não será lida, mas as seguintes iram continuar a ser lidas
     */
    @Test
    public void testReadFile3() throws Exception {
        System.out.println();
        System.out.println("Numero de campos inferior ao exigido:");
        Files instance = new Files(new ActivityList());
        instance.readFile("test/testFile2.txt");
    }

    /**
     * Irá surgir uma mensagem de erro para as linhas cujo o tipo indicado seja
     * invalido, no entanto o erro não irá terminar o programa, a linha não será
     * lida, mas as seguintes iram continuar a ser lidas
     */
    @Test
    public void testReadFile4() throws Exception {
        System.out.println();
        System.out.println("Tipo indicado não existe:");
        Files instance = new Files(new ActivityList());
        instance.readFile("test/testFile3.txt");
    }

    /**
     * Irá surgir uma mensagem de erro para as linhas cujo o campo numerico contenha valor de texto
     * seja inferior ao exigido, no entanto o erro não irá terminar o programa,
     * a linha não será lida, mas as seguintes iram continuar a ser lidas
     */
    @Test
    public void testReadFile5() throws Exception {
        System.out.println();
        System.out.println("Campos numericos com valores em texto:");
        Files instance = new Files(new ActivityList());
        instance.readFile("test/testFile4.txt");
    }

}
