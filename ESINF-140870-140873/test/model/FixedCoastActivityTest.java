package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class FixedCoastActivityTest {
    
    public FixedCoastActivityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTotalCost method, of class FixedCoastActivity.
     */
    @Test
    public void testGetTotalCost() throws Exception {
        System.out.println("getTotalCost");
        Activity at = new Activity("A", "FCA", "Descrição", 3, "weeks");
        FixedCoastActivity instance = new FixedCoastActivity(at, 1000);
        int expResult = 1000;
        int result = instance.getTotalCost();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTotalCost method, of class FixedCoastActivity.
     */
    @Test
    public void testSetTotalCost() throws Exception {
        System.out.println("setTotalCost");
        int totalCost = 1000;
        Activity at = new Activity("A", "FCA", "Descrição", 3, "weeks");
        FixedCoastActivity instance = new FixedCoastActivity(at, 1000);
        instance.setTotalCost(totalCost);
    }
    
}
