package model;

import graph.Graph;
import graph.Vertex;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class PertCpmTest {

    public PertCpmTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getGraph method, of class PertCpm.
     */
    @Test
    public void testGetGraph() {
        System.out.println("getGraph");
        PertCpm instance = new PertCpm();
        Graph expResult = new Graph(true);
        Graph result = instance.getGraph();
        assertEquals(expResult, result);
    }

    /**
     * Test of getEarliestStart method, of class PertCpm.
     */
    @Test
    public void testGetEarliestStart() throws Exception {
        System.out.println("getEarliestStart");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 1);
        expResult.put(new Vertex<Activity, String>(3, c), 3);
        expResult.put(new Vertex<Activity, String>(4, d), 4);
        expResult.put(new Vertex<Activity, String>(5, e), 6);
        instance.calcEarliest();

        HashMap<Vertex<Activity, String>, Integer> result = instance.getEarliestStart();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of getEarliestFinish method, of class PertCpm.
     */
    @Test
    public void testGetEarliestFinish() throws Exception {
        System.out.println("getEarliestFinish");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 1);
        expResult.put(new Vertex<Activity, String>(2, b), 3);
        expResult.put(new Vertex<Activity, String>(3, c), 4);
        expResult.put(new Vertex<Activity, String>(4, d), 6);
        expResult.put(new Vertex<Activity, String>(5, e), 7);
        instance.calcEarliest();

        HashMap<Vertex<Activity, String>, Integer> result = instance.getEarliestFinish();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of getLatestStart method, of class PertCpm.
     */
    @Test
    public void testGetLatestStart() throws Exception {
        System.out.println("getLatestStart");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 1);
        expResult.put(new Vertex<Activity, String>(3, c), 3);
        expResult.put(new Vertex<Activity, String>(4, d), 4);
        expResult.put(new Vertex<Activity, String>(5, e), 6);
        instance.calcEarliest();
        instance.calcLatest();

        HashMap<Vertex<Activity, String>, Integer> result = instance.getLatestStart();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of getLatestFinish method, of class PertCpm.
     */
    @Test
    public void testGetLatestFinish() throws Exception {
        System.out.println("getLatestFinish");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 1);
        expResult.put(new Vertex<Activity, String>(2, b), 3);
        expResult.put(new Vertex<Activity, String>(3, c), 4);
        expResult.put(new Vertex<Activity, String>(4, d), 6);
        expResult.put(new Vertex<Activity, String>(5, e), 7);
        instance.calcEarliest();
        instance.calcLatest();

        HashMap<Vertex<Activity, String>, Integer> result = instance.getLatestFinish();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of getSlack method, of class PertCpm.
     */
    @Test
    public void testGetSlack() throws Exception {
        System.out.println("getSlack");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 0);
        expResult.put(new Vertex<Activity, String>(3, c), 0);
        expResult.put(new Vertex<Activity, String>(4, d), 0);
        expResult.put(new Vertex<Activity, String>(5, e), 0);
        instance.calcEarliest();
        instance.calcLatest();
        instance.calcSlack();

        HashMap<Vertex<Activity, String>, Integer> result = instance.getSlack();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of insertVertex method, of class PertCpm.
     */
    @Test
    public void testInsertVertex() throws Exception {
        System.out.println("insertVertex");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        instance.insertVertex(vertex);
    }

    /**
     * Test of insertEdges method, of class PertCpm.
     */
    @Test
    public void testInsertEdges() throws Exception {
        System.out.println("insertEdges");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        instance.insertVertex(vertex);
        instance.insertEdges(vertex);
    }

    /**
     * Test of calcEarliest method, of class PertCpm.
     */
    @Test
    public void testCalcEarliest() throws Exception {
        System.out.println("calcEarliest");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 1);
        expResult.put(new Vertex<Activity, String>(3, c), 3);
        expResult.put(new Vertex<Activity, String>(4, d), 4);
        expResult.put(new Vertex<Activity, String>(5, e), 6);
        instance.calcEarliest();
    }

    /**
     * Test of calcLatest method, of class PertCpm.
     */
    @Test
    public void testCalcLatest() throws Exception {
        System.out.println("calcLatest");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 1);
        expResult.put(new Vertex<Activity, String>(3, c), 3);
        expResult.put(new Vertex<Activity, String>(4, d), 4);
        expResult.put(new Vertex<Activity, String>(5, e), 6);

        instance.calcEarliest();
        instance.calcLatest();
    }

    /**
     * Test of calcSlack method, of class PertCpm.
     */
    @Test
    public void testCalcSlack() throws Exception {
        System.out.println("calcSlack");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        //ArrayList<Activity> edges = new ArrayList<>();
        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        HashMap<Vertex<Activity, String>, Integer> expResult = new HashMap<>();
        expResult.put(new Vertex<Activity, String>(1, a), 0);
        expResult.put(new Vertex<Activity, String>(2, b), 1);
        expResult.put(new Vertex<Activity, String>(3, c), 3);
        expResult.put(new Vertex<Activity, String>(4, d), 4);
        expResult.put(new Vertex<Activity, String>(5, e), 6);

        instance.calcEarliest();
        instance.calcLatest();
        instance.calcSlack();
    }

    /**
     * Test of activityOrderCompletion method, of class PertCpm.
     */
    @Test
    public void testActivityOrderCompletion() throws Exception {
        System.out.println("activityOrderCompletion");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        instance.calcEarliest();

        Map expResult = new HashMap();
        expResult.put(new Vertex<Activity, String>(1, a), 1);
        expResult.put(new Vertex<Activity, String>(2, b), 3);
        expResult.put(new Vertex<Activity, String>(3, c), 4);
        expResult.put(new Vertex<Activity, String>(4, d), 6);
        expResult.put(new Vertex<Activity, String>(5, e), 7);

        Map result = instance.activityOrderCompletion();

        List<Entry<Vertex<Activity, String>, Integer>> ordenado = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        List<Entry<Vertex<Activity, String>, Integer>> ordenado2 = new ArrayList<Entry<Vertex<Activity, String>, Integer>>(result.entrySet());

        Collections.sort(ordenado2,
                new Comparator<Entry<Vertex<Activity, String>, Integer>>() {
                    @Override
                    public int compare(Entry<Vertex<Activity, String>, Integer> e1, Entry<Vertex<Activity, String>, Integer> e2) {
                        return (-e2.getKey().getElement().getKey().compareTo(e1.getKey().getElement().getKey()));
                    }
                }
        );

        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of allPaths method, of class PertCpm.
     */
    @Test
    public void testAllPaths() throws Exception {
        System.out.println("allPaths");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        Activity start = new Activity("start", "fca", "fixStart", 0, "week");
        Activity finish = new Activity("finish", "fca", "fixFinish", 0, "week");

        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        Deque<Activity> d1 = new LinkedList<>();
        d1.add(start);
        d1.add(a);
        d1.add(b);
        d1.add(d);
        d1.add(e);
        d1.add(finish);
        Deque<Activity> d2 = new LinkedList<>();
        d2.add(start);
        d2.add(a);
        d2.add(b);
        d2.add(c);
        d2.add(d);
        d2.add(e);
        d2.add(finish);

        HashMap<Deque<Activity>, Integer> expResult = new HashMap<>();
        expResult.put(d1, d1.size());
        expResult.put(d2, d2.size());
        HashMap<Deque<Activity>, Integer> result = instance.allPaths();

        List<Entry<Deque<Activity>, Integer>> ordenado = new ArrayList<Entry<Deque<Activity>, Integer>>(expResult.entrySet());

        Collections.sort(ordenado,
                new Comparator<Entry<Deque<Activity>, Integer>>() {
                    @Override
                    public int compare(Entry<Deque<Activity>, Integer> e1, Entry<Deque<Activity>, Integer> e2) {
                        return (e2.getValue().compareTo(e1.getValue()));
                    }
                }
        );

        List<Entry<Deque<Activity>, Integer>> ordenado2 = new ArrayList<Entry<Deque<Activity>, Integer>>(result.entrySet());

        Collections.sort(ordenado2,
                new Comparator<Entry<Deque<Activity>, Integer>>() {
                    @Override
                    public int compare(Entry<Deque<Activity>, Integer> e1, Entry<Deque<Activity>, Integer> e2) {
                        return (e2.getValue().compareTo(e1.getValue()));
                    }
                }
        );
        assertEquals(ordenado, ordenado2);
    }

    /**
     * Test of criticalPath method, of class PertCpm.
     */
    @Test
    public void testCriticalPath() throws Exception {
        System.out.println("criticalPath");
        PertCpm instance = new PertCpm();

        ArrayList<Activity> vertex = new ArrayList<>();
        Activity a = new Activity("A", "FCA", "Descritpion1", 1, "Month");
        ArrayList<Activity> predecessorsB = new ArrayList<>();
        predecessorsB.add(a);
        Activity b = new Activity("B", "FCA", "Descritpion2", 2, "Month", predecessorsB);
        ArrayList<Activity> predecessorsC = new ArrayList<>();
        predecessorsC.add(b);
        Activity c = new Activity("C", "VCA", "Descritpion3", 1, "Month", predecessorsC);
        ArrayList<Activity> predecessorsD = new ArrayList<>();
        predecessorsD.add(b);
        predecessorsD.add(c);
        Activity d = new Activity("D", "VCA", "Descritpion4", 2, "Month", predecessorsD);
        ArrayList<Activity> predecessorsE = new ArrayList<>();
        predecessorsE.add(d);
        Activity e = new Activity("E", "FCA", "Descritpion5", 1, "Month", predecessorsE);
        vertex.add(a);
        vertex.add(b);
        vertex.add(c);
        vertex.add(d);
        vertex.add(e);

        Activity start = new Activity("start", "fca", "fixStart", 0, "week");
        Activity finish = new Activity("finish", "fca", "fixFinish", 0, "week");

        instance.insertVertex(vertex);
        instance.insertEdges(vertex);

        Deque<Activity> d1 = new LinkedList<>();
        d1.add(start);
        d1.add(a);
        d1.add(b);
        d1.add(d);
        d1.add(e);
        d1.add(finish);
        Deque<Activity> d2 = new LinkedList<>();
        d2.add(start);
        d2.add(a);
        d2.add(b);
        d2.add(c);
        d2.add(d);
        d2.add(e);
        d2.add(finish);

        ArrayList<Deque<Activity>> expResult = new ArrayList<>();
        expResult.add(d2);
        expResult.add(d1);
        ArrayList<Deque<Activity>> result = instance.criticalPath();

        Collections.sort(result, new Comparator<Deque<Activity>>() {

            @Override
            public int compare(Deque<Activity> o1, Deque<Activity> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        });
        Collections.sort(expResult, new Comparator<Deque<Activity>>() {

            @Override
            public int compare(Deque<Activity> o1, Deque<Activity> o2) {
                return Integer.valueOf(o1.size()).compareTo(Integer.valueOf(o2.size()));
            }
        });

        assertEquals(expResult, result);
    }

}
