package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 1140870_1140873
 */
public class VariableCostActivityTest {
    
    public VariableCostActivityTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCostTime method, of class VariableCostActivity.
     */
    @Test
    public void testGetCostTime() throws Exception {
        System.out.println("getCostTime");
        Activity at = new Activity("A", "VCA", "Descrição", 3, "weeks");
        VariableCostActivity instance = new VariableCostActivity(at, 2, 50);
        int expResult = 2;
        int result = instance.getCostTime();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCostTime method, of class VariableCostActivity.
     */
    @Test
    public void testSetCostTime() throws Exception {
        System.out.println("setCostTime");
        int costTime = 1;
        Activity at = new Activity("A", "VCA", "Descrição", 3, "weeks");
        VariableCostActivity instance = new VariableCostActivity(at, 2, 50);
        instance.setCostTime(costTime);
    }

    /**
     * Test of getTotalTime method, of class VariableCostActivity.
     */
    @Test
    public void testGetTotalTime() throws Exception {
        System.out.println("getTotalTime");
        Activity at = new Activity("A", "VCA", "Descrição", 3, "weeks");
        VariableCostActivity instance = new VariableCostActivity(at, 2, 50);
        int expResult = 50;
        int result = instance.getTotalTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTotalTime method, of class VariableCostActivity.
     */
    @Test
    public void testSetTotalTime() throws Exception {
        System.out.println("setTotalTime");
        int totalTime = 49;
        Activity at = new Activity("A", "VCA", "Descrição", 3, "weeks");
        VariableCostActivity instance = new VariableCostActivity(at, 2, 50);
        instance.setTotalTime(totalTime);
    }
    
}
